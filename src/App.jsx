import React from "react"

import { ThemeProvider,createTheme } from "@mui/material"

import { BrowserRouter,Route,Routes } from "react-router-dom"

import Header from "./components/Header"

function App() {

  const theme = createTheme ({
    palette:{
      mode:'light',
      primary:{
        main:'#5F021F'
      }
    },
    secondary:{
      main:"#CCCCCC"
    }
  })

  return (
    <ThemeProvider theme={theme}>
      <Header></Header>
    </ThemeProvider>
    
  )
}

export default App
