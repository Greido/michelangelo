import React from 'react'
import { Box } from '@mui/material'
import Select from '@mui/joy/Select';
import Option from '@mui/joy/Option';
function Header() {
    return (
            <>
                    <Select defaultValue="dog">
                        <Option value="dog">Dog</Option>
                        <Option value="cat">Cat</Option>
                    </Select>
            </>
    )
}

export default Header